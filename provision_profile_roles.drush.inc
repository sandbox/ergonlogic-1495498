<?php

/*
 * Implementation of drush_HOOK_post_provision_install().
 *
 * Runs the backend commands to create a second user, send a notification,
 * update the login link, and assign a role.
 */
function drush_provision_profile_roles_post_provision_install() {

drush_log('------------------------------------------------------------');
$myFile = "../../.drush/provision_profile_roles/debug.txt";
$fh = fopen($myFile, 'w') or die("can't open file");
fwrite($fh, print_r(d(), true));
fwrite($fh, drush_get_option('profile_roles_role', ''));
fwrite($fh, drush_get_option('profile_roles_uid1', 'admin'));
fclose($fh);
drush_log('------------------------------------------------------------');


/*

  $role = db_result(db_query('SELECT {role} FROM {hosting_profile_roles_roles} WHERE pl_nid=%d AND pr_nid=%d', $task->ref->platform, $task->ref->profile));
  $uid1 = db_result(db_query('SELECT {uid} FROM {hosting_profile_roles_uid1} WHERE pl_nid=%d AND pr_nid=%d', $task->ref->platform, $task->ref->profile));
 
  $client_name = $task->ref->name;
  $target = $task->context_options['uri'];
  if ($uid1 != FALSE) {

    // if http://drupal.org/node/1116414 gets committed, we can remove the "notify" command code
    $results['user-create'] = provision_backend_invoke($target, 'user-create ' . $client_name . ' --mail=' . user_load(array('name' => $client_name))->mail);
    drush_log(dt('User created:') . $results['user-create']['output']);

    $results['notify'] = provision_backend_invoke($target, 'notify ' . $client_name);
    drush_log($results['notify']);

    $results['user-login'] = provision_backend_invoke($target, 'user-login ' . $client_name);
    drush_log(dt('Client user login link: ') . $results['user-login']['output']);

    // Set the site login link to the one we just created; ref.: hosting_site_post_hosting_login_reset_task()
    $cache = array(
      'expire' => strtotime("+24 hours"),
      'link' => $results['user-login']['output'],
    );
    cache_set('hosting:site:' . $task->ref->nid . ':login_link', $cache, 'cache', $cache['expire'] );
  }
  else {
    // we may want to do this for block-placement, etc.
    $result = provision_backend_invoke($target, 'eval "print(user_load(1)->name)"');
    $client_name = $result['output'];
  }

  if ($role != FALSE) {
    // @todo: support adding multiple roles
    // we can finally check that the role actually exists on this site
    $result = provision_backend_invoke($target, 'eval "print(serialize(user_roles()))"');
    $site_roles = unserialize($result['output']);
    if (in_array($role, $site_roles)) {
      // we need to avoid a role name like ";rm -rf /", which is a valid role name in D6, btw
      drush_escapeshellarg(&$role);
      provision_backend_invoke($target, 'user-add-role ' . $role . ' --name=' . $client_name);
    }
    else {
      drush_set_error(DRUSH_APPLICATION_ERROR, dt('No role of <em>' . $role . '</em> is available on this platform'));
    }
  }*/
}
